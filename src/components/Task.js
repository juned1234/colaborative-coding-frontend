import React,{useState,useEffect} from 'react';
import Axios from "axios"


import { Container,Col,Row,Form,Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './common.css'
import './animation.css'


import Page from './Page';
import Createtask from './Createtask';
//import Jointask from './Jointask';




function Task() {

  

  useEffect(() => {
    document.title = "Task | ShareCode"
    window.scrollTo(0,0)
    
  }, [])

  ///===handler events goes here


  return (
    
<section className="task-section ">
  {/* <Jointask/> */}
 <Createtask/>  
<Page/>
</section>   
  );
}

export default Task;
