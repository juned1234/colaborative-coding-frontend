import React,{useState,useEffect} from 'react';
import {Link,withRouter} from 'react-router-dom';
import Axios from "axios"



import { Container,Col,Row,Form,Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './common.css'
import './animation.css'
import Loginpic from '../images/login-img.png'





function Signup(props) {

  const [name, setName] = useState()
  const [email, setEmail] = useState()
  const [password, setPassword] = useState()

  useEffect(() => {
    document.title = "signup | ShareCode"
    window.scrollTo(0,0)
    
  }, [])

  ///===handler events goes here

async function signupHandler(e){
   
   e.preventDefault()
   // console.log(name,email,password)

   try {
    await Axios.post("http://localhost:8000/api/signup", { name, email, password })
    console.log("User was successfully created.")
    props.history.push(`/Login`)
    setEmail("")
    setName("")
    setPassword("")
  } catch (e) {
    console.log("There was an error.")
  }

   
  //  setEmail("")
  //  setName("")
  //  setPassword("")

  }

  

  return (
    
<section className="login-section ">
    {/* <Header1/> */}
    <div className="dummy-div"></div>
<Container >
  <Row>
    <Col xs={12} md={6}>
    <img className="img-fluid girl-code-img bounce-in-top" src={Loginpic} alt="person-picture" />
    </Col>
    <Col xs={12} md={6}>
    {/* ===form goes from here */}

    <div className="form-area bounce-in-top">
       <h3 className="login-title" style={{textAlign:"center"}}>Sign up</h3>
    <Form onSubmit={signupHandler}>
    <Form.Group >
    <Form.Label>Full Name</Form.Label>
    <Form.Control value={name} onChange={e=>setName( e.target.value)} type="text" placeholder="Enter full name" required />
 
  </Form.Group>

  <Form.Group controlId="formBasicEmail">
    <Form.Label>Email</Form.Label>
    <Form.Control value={email} onChange={e=>setEmail( e.target.value)} type="email" placeholder="Enter email" required />
    {/* <Form.Text className="text-muted">
      We'll never share your email with anyone else.
    </Form.Text> */}
  </Form.Group>

  <Form.Group controlId="formBasicPassword">
    <Form.Label>Password</Form.Label>
    <Form.Control value={password} onChange={e=>setPassword( e.target.value)} type="password" placeholder="Password"  required/>
  </Form.Group>
 <div className="login-button-area" style={{textAlign:"center"}}>
 <Button className="signin-button"  type="submit" >
    Sign up
  </Button>
 </div>
  
</Form>
   <div className="signuplink-route-area" style={{textAlign:"center"}}>
   <p className="signuplink-route" >Already have an account?
    <Link to="/Login" className="Signin-signup-link"> Log in</Link>
</p>
   </div>
    
    </div>

    </Col>
  </Row>

</Container>
      
</section>   
  );
}

export default withRouter(Signup);
