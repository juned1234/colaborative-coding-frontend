import React, { useEffect } from "react"
import {withRouter} from "react-router-dom"

function HeaderLoggedIn(props) {
 // console.log(props.setLoggedIn)
  function handleLogout() {
   // alert("clicking")
    props.setLoggedIn(false)
    localStorage.removeItem("shareCodeappToken")
    localStorage.removeItem("shareCodeappUsername")
    localStorage.removeItem("shareCodeappUseremail")
    localStorage.removeItem("shareCodeappUserdataId")
    props.history.push(`/`)
  
  }

  return (
    <div className="signout-btn-area">
     
     {/* <a onClick={handleLogout} className="Signin-signup-link nav-link header-signin-signup-link" type="button">Log Out</a>  */}
      <button onClick={handleLogout} className="conatct-btn btn btn-primary log-out-btn">
        Sign Out
      </button>
    </div>
  )
}

export default withRouter(HeaderLoggedIn)
