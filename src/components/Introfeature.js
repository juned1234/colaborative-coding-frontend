import React from 'react';
import {Link} from 'react-router-dom';


import {Col,Container,Row } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './common.css'
import Images from '../images/Images'

//console.log(Images[0].src)

function Introfeature (){

    return(
    <>
    {/* intro section starts from here */}

    <section  className="sample-intro-area">

  <Container>
  <Row className="justify-content-md-center">
    <Col>
    <div className="intro-code-girl-image-area" style={{textAlign:"center"}}>
    <img className="code-girl-img" src={require('../images/code3.svg')}  />
    </div>
    {/* <p className="para-intro"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum. </p> */}
				<div className="intro-h3-area">
					<h3 className="intro-h3"> Say Hi To ShareCode :)
              </h3> </div>
    </Col>
  </Row>
</Container>
</section>

      {/*======== feature section starts from here================ */}
        <section className="feature-section ">
   
    <div className="container">
      <h3 className="feature-text-h3">
       Reasons To Use ShareCode
                    
      </h3>
    </div>

    <div className=" feature-area" >
        
    <Container>
 
  <Row>
    <Col xs={12} md={3}>
    <div className="feature-body-img">
      <img className="feature-img" src={require('../images/code.svg')}  />
      </div>
    </Col>
    <Col xs={12} md={9}>
    <div className="feature-body">
	<div className="feature-txt">
		<h5 className="feature-title"> <span className="feature-span-h">Lorem ipsum dolor amet</span></h5>
		<p className="feature-para"> consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
	</div>
</div>
    </Col>

    <Col xs={12} md={3}>
    <div className="feature-body-img">
      <img className="feature-img" src={require('../images/code.svg')} ></img>
      </div>
    </Col>
    <Col xs={12} md={9}>
    <div className="feature-body">
	<div className="feature-txt">
		<h5 className="feature-title"><span className="feature-span-h">Lorem ipsum dolor amet</span></h5>
		<p className="feature-para"> consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
	</div>
</div>
    </Col>

    <Col xs={12} md={3}>
    <div className="feature-body-img">
      <img className="feature-img"  src={require('../images/code.svg')} ></img>
      </div>
    </Col>
    <Col xs={12} md={9}>
    <div className="feature-body">
	<div className="feature-txt">
		<h5 className="feature-title"> <span className="feature-span-h">Lorem ipsum dolor amet</span></h5>
		<p className="feature-para"> consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
	</div>
</div>
    </Col>
  </Row>



</Container>

            </div>
   
  
  </section>
  </>


    )
}

export default Introfeature;