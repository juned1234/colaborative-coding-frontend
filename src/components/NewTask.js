

import React,{useState,useEffect} from 'react';
import Axios from "axios"


import { Container,Col,Row,Form,Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './common.css'
import './animation.css'
import CodeEditor from './CodeEditor';
import WelcomeModal from './WelcomeModal';






function NewTask() {

  

  useEffect(() => {
    document.title = "NewTask | ShareCode"
    window.scrollTo(0,0)
    
  }, [])

  ///===handler events goes here


  return (
    
<section style={{}} className="new-task-section ">
     <WelcomeModal/>
    {/* <div className="dummy-div"></div> */}
     <CodeEditor/>

</section>   
  );
}

export default NewTask;
