import React,{useEffect} from 'react';
import {Link} from 'react-router-dom';
import Header1 from './Header1'
import Contact from './Contact'
import Testimonial from './Testimonial'
import Cta1 from './Cta1'
import Introfeature from './Introfeature'


import { Button,Container,Row,Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './common.css'
import './animation.css'

function Banner (){

   useEffect(() => {
      document.title = "Home | ShareCode"
      window.scrollTo(0,0)
      
    }, [])
  
    ///===handler events goes here

    return(
    <>
<section className="banner-section ">
{/* <Header1/> */}
<div className="dummy-div"></div>
{/* ======banner middle section starts from here==== */}
<Container  className="banner-section-middle">
   <Row className="align-items-center">
      <Col sm={6} className="order-md-first order-last">
      <h1 className="banner-h1">Ultimate Collaborative<br/> <span style={{color: "#595bd0"}}>Coding Tool</span></h1>
      <div>
         <p className="banner-p">Code in Js, Edit, Compile, Chat
         </p>
      </div>
      <div className=" cta2-btn-area" >
         {/* <Button className="cta-btn2" variant="primary"> */}
         <Link to="/Signup" className="try-now-btn"> Try It Now</Link>
         {/* </Button> */}
      </div>
      </Col>
      <Col sm={6} className="text-center order-md-last order-first">
         
      <div className="intro-img-area"> 
      <img className="img-fluid" src={require('../images/development-1.svg')} alt="intro-main-image"/>
      <img className="code-girl-img1 vibrate-1" src={require('../images/code3.svg')}  />
       </div>
      </Col>
   </Row>
</Container>

{/* =========logos area start from here========== */}
<div className="container banner-section-logo-area">
   <div className="featured-section" >
      <div className="row ">
         <div className="col-lg-3 col-md-3 col-sm-6 single-logo">
            <img id="press-logo-id-wjs" className="img-fluid responsive" src={require('../images/js1.png')} alt="js-logo"/>
         </div>
         <div className="col-lg-3 col-md-3 col-sm-6 single-logo">
            <img id="press-logo-id-inc" className="img-fluid responsive" src={require('../images/js2.png')} alt="js-logo" />
         </div>
         <div className="col-lg-3 col-md-3 col-sm-6 single-logo">
            <img id="press-logo-id-verge" className="img-fluid responsive" src={require('../images/js4.png')} alt="js-logo" />
         </div>
         <div className="col-lg-3 col-md-3 col-sm-6 single-logo">
            <img id="press-logo-id-mit" className="img-fluid responsive" src={require('../images/js3.png')} alt="js-logo"/>
         </div>
      </div>
   </div>
</div>
</section>
<Introfeature/>
<Testimonial/>
   <Cta1/>
   <Contact/>
</>

    )
}

export default Banner;