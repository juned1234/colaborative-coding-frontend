// import React,{Component,slider} from 'react';
// import {Link} from 'react-router-dom';
import React, { Component } from "react";
import Slider from "react-slick";

import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";


import 'bootstrap/dist/css/bootstrap.min.css';
import './common.css'


// custom slider arrow functions







export default class Testimonial extends Component {
    constructor(props) {
      super(props);

    }
   
    render() {
        var settings = {
          dots: true,
          infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 2000,
          pauseOnHover: true
        
        };
        return (
            <section className="testimonial-section ">
          <div className="container">
          <h3 className="testimonial-text-h3">Testimonials</h3>

          <div className="carosal-area">
      
          <Slider {...settings}>
              <div className="carosal-card">
                <h3 className="testimonial-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nunc lectus, fringilla eu justo vitae, viverra porta lorem. Cras molestie aliquam mattis.</h3>
              </div>
              <div className="carosal-card">
                <h3 className="testimonial-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nunc lectus, fringilla eu justo vitae, viverra porta lorem. Cras molestie aliquam mattis.</h3>
              </div>
              <div className="carosal-card">
                <h3 className="testimonial-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nunc lectus, fringilla eu justo vitae, viverra porta lorem. Cras molestie aliquam mattis.</h3>
              </div>
             
            </Slider>
       </div>
  
  
            
          </div>
          </section>
        );
      }
  }
  

