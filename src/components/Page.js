import React,{useState,useEffect} from 'react';
import Axios from "axios"


import { Container,Col,Row,Form,Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './common.css'
import './animation.css'






function Page() {

let name = localStorage.getItem("shareCodeappUsername")
//console.log(name);
let name_split = name.split(" ")
//console.log(name_split)
let firstName =name_split[0]


  return (
    
<>
   
    <div className="dummy-div"></div>
<Container >
  
<div className="container container--narrow py-md-5">
  <h2 className="text-center bounce-in-right">Hello <strong style={{color:"#ffc107"}}>{firstName}</strong>, Welcome to <span style={{borderBottom:"3px solid #ffc107",opacity:"0.8"}}>ShareCode.</span></h2>
      {/* <p className="lead text-muted text-center"> consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p> */}
    </div>


</Container>

</>
  );
}

export default Page;
