import React from 'react';
import {Link} from 'react-router-dom';

//import { Button,Navbar,Form,InputGroup,FormControl,Nav,NavDropdown } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './common.css'

function Footer() {
  return (
    <div className="footer">
     <div className="comman-footer-outer">
        <footer className="comman-footer">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="footer-logo">
                            <h3>ShareCode </h3>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <div className="footer-links footer-links-adj">
                            <ul className="list-inline footer-links-inner">
                                <li className="list-inline-item">
                                {/* <Link className="link menu-link" to="/">Home</Link> */}
                                    <a className="link menu-link" href="">Home</a>
                                </li>
                                <li className="list-inline-item">
                                    <a className="link menu-link" href="">About Me</a>
                                </li>
                                                                
                                {/* <li className="list-inline-item">
                                    <a className="link menu-link" href="">Contact Us</a>
                                </li> */}
                            </ul>
                          
                            <ul className="list-inline social-links">
                                <li className="list-inline-item">
                                    <a className="link menu-link linkedin st" href=""><i className="fab fa-twitter fa-lg"></i></a>
                                </li>
                                <li className="list-inline-item">
                                    <a className="link menu-link twitter st" href=""><i className="fab fa-linkedin-in fa-lg"></i></a>
                                </li>
                                
                            </ul>
                        </div>
                        <div className="">
                        {/* <p style={{textAlign:"center",color:"#6c757d !important"}}>Copyright © 2020 ShareCode. All rights reserved.</p> */}
                        </div>
                       
                    </div>
                </div>
            </div>
        </footer>
    </div>
  
    </div>
  );
}

export default Footer;
