import React,{useState} from 'react';
import Axios from "axios"

import { Button,Form,InputGroup,FormControl,Container,Row,Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './common.css'

function Contact() {

    const [name, setName] = useState()
    const [email, setEmail] = useState()
    const [message, setMessage] = useState()
    const [mobile, setMobile] = useState()
  
  async function contactHandler(e){
     
     e.preventDefault()
     console.log(name,email,message)

     try {
      await Axios.post("http://localhost:8000/api/contact", { name, email,message })
      console.log("message succesfully sent...")
    } catch (e) {
      console.log("There was an error unable to send message...")
    }
     
    //  setEmail("")
    //  setName("")
    //  setMsg("")
  
    }

  return (
    <div className="contact-area">

<Container>
    <h2 className="contact-header" >Contact Form</h2>

    
<Form onSubmit={contactHandler}>

<Row>
    <Col sm={6}><Form.Group controlId="exampleForm.ControlInput1" className="control-input">
    {/* <Form.Label>Name</Form.Label> */}
    <Form.Control value={name} onChange={e=>setName(e.target.value)} type="text" placeholder="Name*" required />
  </Form.Group>
  <Form.Group controlId="exampleForm.ControlInput1" className="control-input">
    {/* <Form.Label>Email address</Form.Label> */}
    <Form.Control value={email} onChange={e=>setEmail(e.target.value)}  type="email" placeholder="Email*" required/>
  </Form.Group>
  <Form.Group controlId="exampleForm.ControlInput1" className="control-input">
    {/* <Form.Label>Email address</Form.Label> */}
    <Form.Control value={mobile} onChange={e=>setMobile(e.target.value)}  type="phone" placeholder="Phone" />
  </Form.Group>
  </Col>

    <Col sm={6}>  <Form.Group controlId="exampleForm.ControlTextarea1">
    {/* <Form.Label>Example textarea</Form.Label> */}
    <Form.Control value={message} onChange={e=>setMessage(e.target.value)}  as="textarea" placeholder="Write here..." rows="6" />
  </Form.Group>
  {/* submit button goes here */}
  <div className="contact-btn-area" >
  <Button className="conatct-btn" variant="primary" type="submit">
    Contact Us
  </Button>
  </div>
  </Col>
  </Row>
  



</Form>
  
</Container>
   
    </div>
  );
}

export default Contact;
