import React from "react"
import Testing from "./context/Testing-context"

function Test(props) {
    return (
        <Testing.Consumer>

           {theme => {
                return (
                  <div style={{height:"100vh",textAlign:"center"}}>
                    <button style={{marginTop: "20%"}} >{`${theme}-theme`}</button>
                  </div>
                  
                )
            }}
          

            
        </Testing.Consumer>
        
    )    
}

export default Test