import React,{useState,useEffect} from 'react';
import Axios from "axios"
import {Link,withRouter} from 'react-router-dom';


import { Container,Col,Row,Form,Button,Modal } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './common.css'
import './animation.css'


  
  function JoinTask(props) {
    const [taskid, setTaskid] = useState()

    //===event handler goes here
  function handleSubmit(e) {
       
      e.preventDefault()
         console.log(taskid)
         props.history.push(`/task/${taskid}`)
    //     try {
    //    const result = await Axios.get("http://localhost:8000/api/task", { task:title, email:localStorage.getItem("shareCodeappUseremail"),user_id:localStorage.getItem("shareCodeappUserdataId") })
    //       console.log(result.data)
    //       let taskid = result.data._id;
    //       // Redirect to new task url
    //       props.history.push(`/task/${taskid}`)

    //     } catch (e) {
    //       console.log("There was an error unable to join...")
    //     }

        
      }
  
    return (
      <>
      <div className="create-task-form-area">
      <Form onSubmit={handleSubmit} className="bounce-in-left">
      <Form.Group >
    <h2>Join</h2>
    <Form.Control value={taskid} onChange={e=>setTaskid( e.target.value)} type="text" placeholder="Enter id here" required  />
 
  </Form.Group>
  <Button className="signin-button"  type="submit" >
    join sharecode
  </Button>
     </Form>
      </div>
      </>
    );
  }

  export default withRouter(JoinTask)