import React,{useState} from 'react';
import {Link} from 'react-router-dom';

import { Button,Navbar,Form,InputGroup,FormControl,Nav,NavDropdown,Container } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './common.css'
import  HeaderLoggedIn from './LoggedIn'
import Login from './Login';

function Header1(props) {
 // console.log(props)
 
  

  return (
    <div className="header1 ">
        {/* <Container> */}
  <Navbar collapseOnSelect expand="lg" variant="light" bg="light" fixed="top">
      <div className="container">
      {/* <Navbar.Brand href="#home" className="brand">ShareCode</Navbar.Brand> */}
      <Link to="/" className="brand navbar-brand">ShareCode</Link>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
 
    <Nav className="ml-auto">
      {/* <Nav.Link href="#deets">More deets</Nav.Link> */}
      
      {props.loggedIn ? <HeaderLoggedIn setLoggedIn={props.setLoggedIn} /> : <Link to={{ pathname: '/Signup' }} className="Signin-signup-link nav-link header-signin-signup-link"> Log In / Sign Up</Link> }

    </Nav>
  </Navbar.Collapse>
      </div>
   
  </Navbar>
{/* </Container> */}


    </div>
  );
}

export default Header1;
