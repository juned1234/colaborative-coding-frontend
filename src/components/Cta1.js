import React from 'react';
import {Link} from 'react-router-dom';


import { Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './common.css'

function Cta1 (){

    return(
    
        <section className="cta-section ">
   
    <div className="container">
      <h3 className="cta-text-h3">
                        Take the first step towards collaborative coding
                    
      </h3>
    </div>

    <div className=" cta-btn-area" >
    {/* <Button className="cta-btn1" variant="primary"> */}
    <Link to="/Signup" className="try-now-btn"> Let's Code</Link>
    {/* </Button> */}
            </div>
   
  
  </section>


    )
}

export default Cta1;