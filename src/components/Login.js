import React,{useState,useEffect} from 'react';
import {Link,withRouter} from 'react-router-dom';
import Axios from "axios"


import { Container,Col,Row,Form,Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './common.css'
import './animation.css'
import Loginpic from '../images/login-img.png'



function Login(props) {
 // console.log(props.setLoggedIn)

  const [email, setEmail] = useState()
  const [password, setPassword] = useState()

  useEffect(() => {
    document.title = "login | ShareCode"
    window.scrollTo(0,0)
    
  }, [])

  ///===handler events goes here
async function signinHandler(e){
   
   e.preventDefault()
   //console.log(email,password)
   try {
    const response = await Axios.post("http://localhost:8000/api/signin", { email, password })
    // if (response.data) {
      console.log(response.data)
      localStorage.setItem("shareCodeappToken", response.data.token)
      localStorage.setItem("shareCodeappUsername", response.data.user.name)
      localStorage.setItem("shareCodeappUseremail", response.data.user.email)
      localStorage.setItem("shareCodeappUserdataId", response.data.user._id)
      props.setLoggedIn(true)
      props.history.push(`/`)

    // } else {
    //   console.log("Incorrect username / password.")
    // }
  } catch (error) {
    console.log("There was a problem.",error)
  }

   
  //  setEmail("")
  //  setPassword("")

  }


  return (

 
    
<section className="login-section ">
    {/* <Header1 checkinlogin ={checkinlogin}/> */}
    <div className="dummy-div"></div>
<Container >
  <Row>
    <Col xs={12} md={6}>
    <img className="img-fluid girl-code-img bounce-in-top" src={Loginpic} alt="person-picture" />
    </Col>
    <Col xs={12} md={6}>
    {/* ===form goes from here */}

    <div className="form-area bounce-in-top">
       <h3 className="login-title" style={{textAlign:"center"}}>Login</h3>

  <Form onSubmit={signinHandler}>
  <Form.Group controlId="formBasicEmail">
    <Form.Label>Email</Form.Label>
    <Form.Control value={email} onChange={e=>setEmail( e.target.value)} type="email" placeholder="Enter email" required />
    {/* <Form.Text className="text-muted">
      We'll never share your email with anyone else.
    </Form.Text> */}
  </Form.Group>

  <Form.Group controlId="formBasicPassword">
    <Form.Label>Password</Form.Label>
    <Form.Control value={password} onChange={e=>setPassword( e.target.value)} type="password" placeholder="Password"  required/>
  </Form.Group>
 <div className="login-button-area" style={{textAlign:"center"}}>
 <Button className="signin-button"  type="submit" >
    Log in
  </Button>
 </div>
  
</Form>
   <div className="signuplink-route-area" style={{textAlign:"center"}}>
   <p className="signuplink-route" >Don't have an account?
    <Link to="/Signup" className="Signin-signup-link"> Sign up </Link>
</p>
   </div>
    
    </div>

    </Col>
  </Row>

</Container>
      
</section>   

  );
}

export default withRouter(Login);
