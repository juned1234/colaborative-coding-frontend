import React from 'react';

import AceEditor from "react-ace";
 
import "ace-builds/src-noconflict/mode-javascript";
import "ace-builds/src-noconflict/theme-monokai";
import "ace-builds/src-noconflict/ext-language_tools"

import { Container,Col,Row,Form,Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './common.css'
import './animation.css'





function CodeEditor() {

    // function onChange(newValue) {
    //     //console.log("change", newValue);
    //   }

      return (
        
        <section className="task-section ">
   
        <div className="dummy-div"><h3>Let's start coding </h3></div>
    <Container fluid >
      <Row>
        <Col xs={12} md={7}>

            {/* aceeditore goes here */}
            <AceEditor
    mode="javascript"
    theme="monokai"
    fontSize={16}
    width="100%"
    
    // onChange={onChange}
    name="UNIQUE_ID_OF_DIV"

    editorProps={{ $blockScrolling: true }}

    // value={`function onLoad(editor) {
    //     console.log("i've loaded");
    //   }
    //   //=========lets code together ========//
    //    `}

       setOptions={{
        enableBasicAutocompletion: true,
        enableLiveAutocompletion: true,
        enableSnippets: false,
        showLineNumbers: true,
        tabSize: 2,
        }}
        
    />
        </Col>
        <Col xs={12} md={5}>
       
    
        <div className="chat-videocall-area">
         
         </div>
         {/* chat screen from here */}
          
      <div className="chat-wrapper chat-shadow border-top">

      </div>
 
    
        </Col>
      </Row>
    
    </Container>
          
    </section> 

      );
    }
    
    export default CodeEditor;
    