import React,{useState} from 'react';

import { Container,Col,Row,Form,Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './common.css'


function Homebody() {

  const [name, setName] = useState()
  const [email, setEmail] = useState()
  const [password, setPassword] = useState()

function signupHandler(e){
   
   e.preventDefault()
   console.log(name,email,password)
   
   setEmail("")
   setName("")
   setPassword("")

  }



  return (
    
     <Container className="homebody-area">
  <Row>
  <Col xs={12} md={7} className="intro-block">
  <h1 className="home-intro-title">Share Code in Real-time</h1>
   <p className="home-intro-describe">It is an online real-time collaborative code editor and compiler. Our web-based application allows users to collaborate in real-time over the internet.</p>
  </Col>
    
    <Col xs={12} md={5}>
      <div className="register-form-area">

      <Form onSubmit={signupHandler} className="register-card">

      <Form.Group controlId="formGroupEmail">
    <Form.Label>Name</Form.Label>
    <Form.Control value={name} onChange={e=>setName( e.target.value)} type="text" placeholder="Enter name" required />
  </Form.Group>        
  <Form.Group controlId="formGroupEmail">
    <Form.Label>Email address</Form.Label>
    <Form.Control value={email} onChange={e=>setEmail( e.target.value)} type="email" placeholder="Enter email" required />
  </Form.Group>
  <Form.Group controlId="formGroupPassword">
    <Form.Label>Password</Form.Label>
    <Form.Control value={password} onChange={e=>setPassword( e.target.value)} type="password" placeholder="Password" required />
  </Form.Group>
  {/* <Form.Group controlId="formGroupPassword">
    <Form.Label>Confirm Password</Form.Label>
    <Form.Control type="password" placeholder="Password" />
  </Form.Group> */}
  <diV style={{textAlign:"center"}}>
  <Button  variant="outline-success" type="submit">sign up</Button>
  </diV>
</Form>

      </div>


    </Col>
  </Row>
</Container>

  );
}

export default Homebody;
