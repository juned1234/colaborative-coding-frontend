import React,{useState} from 'react';
import {Route,Redirect} from 'react-router-dom'
// import './App.css';
import Footer from './components/Footer'
// import About from './components/About'

import Banner from './components/Banner'
import Login from './components/Login'
import Signup from './components/Register'
import Task from './components/Task';
import Header1 from './components/Header1';
//import Testing from './components/context/Testing-context';
import Test from './components/testing';
import NewTask from './components/NewTask';
import CreateTask from './components/Createtask';

import io from 'socket.io-client';

function App() {
  const [loggedIn, setLoggedIn] = useState(Boolean(localStorage.getItem("shareCodeappToken")))
   

  return (
    <>
    <Header1 setLoggedIn={setLoggedIn} loggedIn={loggedIn}/>
    {/* <Banner/> */}
   
    <Route path="/Login"   component={() => <Login setLoggedIn={setLoggedIn} />} />
     <Route path="/Signup"   component={Signup}/>
     <Route path="/" exact>
      {loggedIn? <Task/>:<Banner/>}
     </Route>
     <Route path="/task/:id">
          <NewTask />
        </Route>
        <Route path="/create-task">
          <CreateTask/>
        </Route>
     
     <Route path="/Test"  component={Test} />
     <Redirect to="/"/>
   {/* <Signup/> */}
   
  <Footer/>
  


    </>
  );
}

export default App;
